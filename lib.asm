section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
  %define SYS_EXIT 60   
    mov rax, SYS_EXIT
    mov rdi, 0
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
    %define NULL_TERMINATOR 0

string_length:
    xor rax, rax

.loop:
    cmp byte [rdi+rax], NULL_TERMINATOR 
    je .end
    inc rax
    jmp .loop

.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
%define SYS_WRITE 1        
%define STDOUT_D 1       
print_string:
    push rdi             
    call string_length    
    pop rsi               
    mov rdx, rax         
    mov rax, SYS_WRITE    
    mov rdi, STDOUT_D    
    syscall               
    ret                   


; Принимает код символа и выводит его в stdout
%define SYS_WRITE 1       
%define STDOUT_D 1       
%define CHAR_SIZE 1        

print_char:
    push rdi              
    mov rdx, CHAR_SIZE     
    mov rsi, rsp           
    mov rax, SYS_WRITE    
    mov rdi, STDOUT_D     
    syscall               
    pop rdi               
    ret                    


; Переводит строку (выводит символ с кодом 0xA)
%define SYS_WRITE 1        
%define STDOUT_D 1        
%define CHAR_SIZE 1        

print_newline:
    mov rdi, 0xA           
    push rdi              
    mov rdx, CHAR_SIZE     
    mov rsi, rsp           
    mov rax, SYS_WRITE     
    mov rdi, STDOUT_D     
    syscall                
    pop rdi                
    ret                    


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
      push rbp                     ; Сохраняем указатель базы
    mov rbp, rsp                 ; Настраиваем стек

    sub rsp, 32                  ; Резервируем место для временных данных (локальных переменных)

    mov rax, rdi                 ; Копируем число в rax
    mov rcx, 10                  ; Основание десятичной системы
    lea rsi, [rsp + 31]          ; Указываем на конец выделенного на стеке региона
    mov byte [rsi], 0            ; Добавляем нуль-терминатор

convert_loop:
    xor rdx, rdx                 ; Очищаем регистр для результата деления
    div rcx                      ; rax / 10, результат в rax, остаток в rdx
    add dl, '0'                  ; Преобразуем остаток в ASCII
    dec rsi                      ; Смещаем указатель на буфер
    mov [rsi], dl                ; Сохраняем символ в буфер
    cmp rax, 0                ; Проверяем, деление завершено?
    jnz convert_loop             ; Если rax != 0, продолжаем цикл

    mov rdi, rsi                 ; Передаем указатель на начало строки в rdi для печати
    call print_string            ; Вызываем функцию печати строки

    add rsp, 32                  ; Восстанавливаем стек
    pop rbp                      ; Восстанавливаем базовый указатель
    ret           
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0             
    jge .positive               

    
    push rdi                   
    mov rdi, '-'               
    call print_char            
    pop rdi                    
    neg rdi                   
    jmp print_uint            

.positive:

    jmp print_uint             


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
%define IS_EQUAL_RET 1  

string_equals:
    xor rcx, rcx             
    xor rax, rax             
.loop:
    mov al, byte [rdi + rcx] 
    cmp al, byte [rsi + rcx] 
    jne .not                
    cmp al, 0             
    je .equal                
    inc rcx                  
    jmp .loop                

.not:
    xor rax, rax             
    ret

.equal:
    mov rax, IS_EQUAL_RET 
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
%define SYS_READ 0          
%define READ_SIZE 1         
%define STDIN 0             ; Дескриптор для стандартного ввода (stdin)

read_char: 
    xor rax, rax            ; Очищаем rax для системного вызова SYS_READ
    mov rdi, STDIN          ; Устанавливаем rdi на дескриптор stdin (0)
    lea rsi, [rsp-1]        ; Выделяем место на стеке для символа (работаем с rsp - 1)
    mov rdx, READ_SIZE      ; Читаем 1 байт
    syscall                 ; Вызов системного вызова

    cmp rax, 0           ; Проверяем результат системного вызова
    jz .end                 ; Если rax == 0 (конец потока), переходим на .end
    js .error               ; Если rax < 0, произошла ошибка

    movzx rax, byte [rsp-1] ; Загружаем прочитанный байт в rax
    ret

.end:
    xor rax, rax            ; Возвращаем 0 в случае конца потока
    ret

.error:
    mov rax, -1             ; Возвращаем -1 в случае ошибки
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
%define BUF_PTR r12    ; Регистр для указателя на буфер
%define BUF_SIZE r13   ; Регистр для размера буфера
%define CHAR_COUNT r14 ; Регистр для подсчета символов

read_word:
    push BUF_PTR
    push BUF_SIZE
    push CHAR_COUNT    ; сохранение используемых callee-saved регистров

    mov BUF_PTR, rdi   ; BUF_PTR = адрес буфера
    mov BUF_SIZE, rsi  ; BUF_SIZE = размер буфера
    dec BUF_SIZE       ; одно место резервируем под нуль-терминатор

    xor CHAR_COUNT, CHAR_COUNT  ; счетчик прочитанных символов

.loop_empty_start:
    call read_char  ; читаем символ

    cmp rax, 0      ; строка пуста
    je .good
    cmp rax, 33     ; непечатаемый символ (код пробела - 32)
    jb .loop_empty_start

    mov byte [BUF_PTR + CHAR_COUNT], al  ; не пробельный символ, помещаем в буфер
    inc CHAR_COUNT

.loop:
    cmp BUF_SIZE, CHAR_COUNT  ; длина строки уже превысила размер буфера - 1 (последний байт зарезервирован под 0)
    jl .bad 

    call read_char

    cmp rax, 33  ; пробельный символ = конец слова
    jb .good

    mov byte [BUF_PTR + CHAR_COUNT], al  ; сохранение текущего символа в буфер
    inc CHAR_COUNT

    jmp .loop
    
.good:
    mov byte [BUF_PTR + CHAR_COUNT], 0  ; добавление нуля (терминанта)
    mov rax, BUF_PTR                    ; указатель на начало буфера
    mov rdx, CHAR_COUNT                 ; получившаяся длина буфера
    jmp .end

.bad:
    xor rax, rax  ; при неудаче возвращаем 0
    
.end:
    pop CHAR_COUNT
    pop BUF_SIZE
    pop BUF_PTR   ; восстановление использованных callee-saved регистров
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
 xor rax, rax
   xor rdx, rdx
   cmp rdi, 0
   jz .end
   .loop:
        movzx rcx, byte [rdi]
	    sub cl, '0'
	    cmp cl, 9
	    ja .end
	    imul rax, rax, 0xA
	    add rax, rcx
	    inc rdi
	    inc rdx
	    jmp .loop
    .end:
	    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; Определяем константы
%define NEGATIVE_SIGN '-'
%define POSITIVE_SIGN '+'
%define ASCII_ZERO '0'
%define ASCII_NINE '9'

section .text

parse_int:
    xor rax, rax                    ; Сбрасываем результат (rax)
    xor rdx, rdx                    ; Сбрасываем длину (rdx)

    cmp byte [rdi], NEGATIVE_SIGN    ; Проверяем, является ли знак отрицательным
    je .negative                     ; Если отрицательный, переход к обработке
    cmp byte [rdi], POSITIVE_SIGN    ; Проверяем на положительный знак
    je .positive                     ; Если положительный, просто переходим к положительному

    ; Проверяем, является ли число нулем или положительным числом
    cmp byte [rdi], ASCII_ZERO       ; Проверяем, является ли первый символ '0'
    jb .invalid                      ; Если < '0', ошибка
    cmp byte [rdi], ASCII_NINE       ; Проверяем, является ли первый символ '9'
    ja .invalid                      ; Если > '9', ошибка

    ; Обрабатываем положительное число
    jmp .positive                     ; Переходим к парсингу числа

.negative:
    inc rdi                         ; Пропускаем знак '-'
    sub rsp, 8                       ; Выделяем место на стеке
    call parse_uint                  ; Вызываем функцию для парсинга числа
    add rsp, 8                       ; Освобождаем место на стеке

    cmp rdx, 0                    ; Проверяем, успешно ли распарсили число
    jz .end                          ; Если нет, завершаем с ошибкой
    inc rdx                          ; Увеличиваем длину на 1 (для знака '-')
    imul rax, -1                     ; Меняем знак на отрицательный
    jmp .end                         ; Завершаем

.positive:
    sub rsp, 8                       ; Выделяем место на стеке
    call parse_uint                  ; Вызываем функцию для парсинга числа
    add rsp, 8                       ; Освобождаем место на стеке
    ; Длина (rdx) остается неизменной для положительного числа

.end:
    ret                               ; Возвращаем результат

.invalid:
    xor rax, rax                     ; Возвращаем 0 (ошибка)
    xor rdx, rdx                     ; Длина = 0
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: 
    xor rax, rax				
    .loop:
    	cmp     rax, rdx				
    	jge     .false
    	mov     r8b, byte [rdi+rax]
    	mov     byte [rsi+rax], r8b			
    	cmp r8b,0		
    	inc     rax				
    	jne     .loop 
    	ret
    .false:
    	xor     rax, rax
    	ret
